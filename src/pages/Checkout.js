import '../styles/Checkout.css';
import {Fragment, useContext, useState, useEffect} from 'react';
import Container from '../components/Container';
import {Form, Button} from 'react-bootstrap';
import {Link, useNavigate} from 'react-router-dom';
import UserContext from '../userContext';
import Swal from 'sweetalert2';

export default function Checkout(){
	const {user, counter, setCounter} = useContext(UserContext);
	const history = useNavigate();
	const [productCheckOut, setProductCheckOUt] = useState();
	let [totalAmount, setTotalAmount] = useState(0);
	let [userInfo, setUserInfo] = useState([]) 
	const [productList, setProductList] = useState()
	let [checkout, setCheckOut] = useState(false);

	const token = localStorage.getItem('token');
	const userId = user.id;
	useEffect(() => {
		fetch('https://safe-bastion-71965.herokuapp.com/user/orders',{
			method: 'GET',
			headers: {
				'Content-Type':'application/json',
				Authorization: `Bearer ${token}` 
			}
		})	
		.then(res => res.json())
		.then(data => {
			const items = data.filter(item => item.userId === userId && item.status === 'pending')
			setProductList(items)
			const checkOutItems = items.map(item => {
				setTotalAmount(totalAmount += item.price)
				return <div key={item._id}>
					<div className="checkout-productsBody">
						<div>
							<p className="checkout-product__name">{item.productName}</p>
							<p className="checkout-product__pieces">{item.qty} piece/s</p>
						</div>
						<p className="checkout-product__price">₱{item.price}</p>
					</div>
				</div>	
			}) 
			setProductCheckOUt(checkOutItems)
		});		
	}, [])

	useEffect(() => {
		fetch('https://safe-bastion-71965.herokuapp.com/user/profile',{
			method: 'GET',
			headers: {
				'Content-Type':'application/json',
				Authorization: `Bearer ${token}` 
			}
		})	
		.then(res => res.json())
		.then(data => {
				setUserInfo([data].map(userInfo => {
					return  <p key={userInfo._id} className="checkout__address">
						{
							userInfo.address[0].lotNumber + ' ' + userInfo.address[0].barangay + ' ' +
							userInfo.address[0].city + ' ' + userInfo.address[0].provice + ' ' +
							userInfo.address[0].country
						}
					</p>
				}))				
			
		})
	}, [])

	const SuccessAlertUpdate = () => {
		Swal.fire({
			title:'FarmersPH.',
			text: 'Item Checkout Successfully',
			icon: 'success',
			showConfirmButton:false
		})	
	}

	const ErrorAlert = () => { 
		Swal.fire({
			title:'FarmersPH.',
			text: 'Failed to Checkout Item',
			icon: 'error',
			confirmButtonText: 'Try again'
		})
	}

	const checkOutFunc = () => {
		productList.map(product => {
			fetch(`https://safe-bastion-71965.herokuapp.com/user/add-to-cart/${product._id}`,{
				method: 'PATCH',
				headers: {
					'Content-Type':'application/json',
					Authorization: `Bearer ${token}`
				},
				body: JSON.stringify({
					status:'checked-out'
				})
			})
			.then(res => res.json())
			.then(data => {
				if([data].length) setCheckOut(true)
			})
		})
		SuccessAlertUpdate()
		history('/products')
		setCounter(counter + 1)

	}
	

	return <Fragment>
		<Container>
			<div className="checkout-container">
				<h1 className="checkout__title">Checkout Details</h1>
				<div className="checkout__header">
					<p className="checkout__header-title">Items</p>
					<p className="checkout__header-title">Sub-Total Price</p>
				</div>
				{productCheckOut}
				<div className="checkout-productTotal">
					<p className="checkout-productTotal__totalAmt">Total Amount</p>
					<p className="checkout-productTotal__ttlPrice">₱{totalAmount}</p>
				</div>
				<div className="checkout__deliveryAddressBox">
					<p className="checkout__deliveryAddress">Delivery Address</p>
					{userInfo}
				</div>
				<div className="checkout__payment-box">
					<p className="checkout__paymentTitle">Mode of Payment</p>
					<p className="checkout__payment">COD(Cash On Delivery)</p>
				</div>
				<div className="checkout__processBtn">
					<Button  as={Link} to="/cart" exact="true" variant="danger" className="checkout__btn">Cancel</Button>
					<Button variant="dark" className="checkout__btn" onClick={checkOutFunc}>Checkout</Button>
				</div>
			</div>
		</Container>
	</Fragment>
}