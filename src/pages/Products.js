import '../styles/Products.css';

import { Fragment, useState, useEffect, useContext } from 'react';
import Cards from '../components/Cards';
import { Row, Col } from 'react-bootstrap';
import UserContext from '../userContext';

export default function Product(){
	const {counter, setCounter} = useContext(UserContext);
	const [products, setProducts] = useState([])

	useEffect(() => {
		fetch('https://safe-bastion-71965.herokuapp.com/product',{
			method: 'GET',
			headers: { 'Content-Type':'application/json'}, 
		})
		.then(res => res.json())
		.then(data => {
			const productList = data.filter(item => item.isActive === true)
			setProducts(productList);
		})	
	},[counter])


	const Products = products.map(product => {
		const productArrName = product.name;
		const productName = productArrName
		const imageUpload = `https://safe-bastion-71965.herokuapp.com/${product.productImage}`
		return <Col md={4} key={product._id}> 
			<div className="product-box">
			<Cards id={product._id} title={productName} price={product.price} image ={imageUpload}/> 
			</div>
		</Col>
	})

	return <Fragment>
		<div className="product-box-container">
			<p className="products-header">Farm Products</p>
			<Row className="">
				{(products)? Products: <h1>Market is empty</h1> }
			</Row>
		</div>
	</Fragment>
	
}				