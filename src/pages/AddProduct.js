import '../styles/AddProduct.css';
import {useState, useEffect, useContext, Fragment} from 'react';
import {Form, Row, Col, Button} from 'react-bootstrap';
import {useNavigate, Navigate, Link} from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../userContext';


export default function AddProduct () {

	const {user} = useContext(UserContext);
	const history = useNavigate();

	const [name, setName] = useState('');
	const [type, setType] = useState('');
	const [description, setDescription] = useState('');
	const [numberOfStocks, setNumberOfStocks] = useState('');
	const [price, setPrice] = useState('');
	const [imageFile, setImageFile] = useState([]);
	const [savedProd, setSavedProd] = useState([]);
	const [isActive, setIsActive] = useState(false);
	const [formDetails, setFormDetails] = useState({});


	const onChangeFileHandler = (e) =>  {
		console.log(e.target.files[0]);
		setImageFile(e.target.files[0]);

	}

	function registerProd(e) {
		e.preventDefault();
		const formData = new FormData();
		formData.append('name', name);
		formData.append('type', type);
		formData.append('price', price);
		formData.append('description', description);
		formData.append('numberOfStocks', numberOfStocks);
		formData.append('availableStock', numberOfStocks);
		formData.append('productImage', imageFile);

		fetch('https://safe-bastion-71965.herokuapp.com/product/register', {
			method: 'POST',
			body: formData,

		})
		.then(res => res.text())
		.then(prod => {
			console.log(prod)
			if(prod !== 'undefined') {

				Swal.fire({
					title: 'Success!',
					icon: 'success',
					text: 'Product is Successfully Added'
				});
			setSavedProd(prod);
			history('/admin');

			} else {
				Swal.fire({
				title: 'Something wrong',
				icon: 'error',
				text: 'Please try again.'
			})
			}
		})

		setName('');
		setType('');
		setDescription('');
		setPrice('');
		setNumberOfStocks('');

	}


	

	useEffect(() => {
		fetch('https://safe-bastion-71965.herokuapp.com/product', {
			headers: {
				'Content-Type': 'application/json'
			}
		})
		.then(res => res.json())
		.then(data => setSavedProd(data));

		if(name && type  && price && description  && numberOfStocks !== ''){
			setIsActive(true)
		} else {
			setIsActive(false)
		}
	}, [name, type, price, description, numberOfStocks, setSavedProd])

	return (
	<Fragment>
		<div className="signup-general-information">
		<p className="sign-up__header">Product Information</p>
		<Form onSubmit={(e) => registerProd(e)}>
		<Row>
		  <Col>
		  <Form.Group className="mb-3">
		    <Form.Label className="form-label-header">Product Name</Form.Label>
		    <Form.Control 
		    	onChange={e => setName(e.target.value)} 
		    	size="sm" 
		    	type="text" 
		    	value = {name}
		    	required/>
		  </Form.Group>
		  </Col>
		  <Col>
		  <Form.Group className="mb-3" controlId="formBasicEmail">
		    <Form.Label className="form-label-header">Type</Form.Label>
		    <Form.Control 
		    	onChange={e => setType(e.target.value)} 
		    	size="sm" 
		    	type="text" 
		    	value = {type}
		    	required />
		  </Form.Group>
		  </Col>
		  </Row>
		  <Form.Group className="mb-3" controlId="formBasicEmail">
			    <Form.Label className="form-label-header">Description</Form.Label>
			    <div className="signup-address-input">
			    <Form.Control  
			    	onChange={e => setDescription(e.target.value)}  
			    	size="sm" as="textarea" 
			    	value = {description}
			    	required />
		   		</div>
		  </Form.Group>
		  <Row>
		  <Col>
		  <Form.Group className="mb-3" controlId="formBasicEmail">
		    <Form.Label className="form-label-header">Quantity</Form.Label>
		    <Form.Control 
		    	onChange={e => setNumberOfStocks(e.target.value)} 
		    	size="sm" 
		    	type="text" 
		    	value= {numberOfStocks}
		    	required/>
		  </Form.Group>
		  </Col>
		  <Col>
		  <Form.Group className="mb-3" controlId="formBasicEmail">
		    <Form.Label className="form-label-header">Price</Form.Label>
		    <Form.Control 
		    	onChange={e => setPrice(e.target.value)} 
		    	size="sm" 
		    	type="text" 
		    	value = {price}
		    	required />
		  </Form.Group>
		  </Col>
		  </Row> 
		  <Form.Group controlId="formFileSm" className="mb-3">
		  <Form.Label>Upload file for Image</Form.Label>
		  <Form.Control type="file" size="sm" onChange = {onChangeFileHandler}/>
		  </Form.Group>
		  <div>
		  	<Button variant = 'primary' className = 'cancel_btn mx-3' as = {Link} to = '/admin'>Cancel</Button>
		  	{(isActive)?
		  	<Button variant = 'primary' className = 'addProduct_btn mx-3' type = "submit">Add Product</Button>
		  	:
		  	<Button variant = 'light' className = 'addProduct_btn mx-3' type = "submit" disabled>Add Product</Button>}
		  </div>
		  </Form>
		  </div>

	</Fragment>
		)
}