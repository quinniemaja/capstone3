import '../styles/ProductDetail.css';
import { Fragment, useState, useEffect, useContext } from 'react';
import { Row, Col, Button, Image } from 'react-bootstrap';
import {useParams, useNavigate, Link} from 'react-router-dom';
import Rating from './Rating';
import UserCOntext from '../userContext';
import Swal from 'sweetalert2';

export default function ProductDetail(props){

	const {user,counter, setCounter} = useContext(UserCOntext);
	const {productId} = useParams();
	const [product, setProduct] = useState({})
	const history = useNavigate()
	let {description, name, stocked, _id, price} = product 

	const availableStocks = stocked;
		useEffect(() => {
		fetch(`https://safe-bastion-71965.herokuapp.com/product/${productId}`,{
			method: 'GET',
			headers: { 'Content-Type':'application/json'}, 
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
			setProduct(data);
		})	
	},[setProduct])
	
	let imageUpload = `https://safe-bastion-71965.herokuapp.com/${product.productImage}`
	let [totalBought, setTotalBought] = useState(0)
	let [totalAmount, setTotalAmount] = useState(0);

	function addProduct(){
			setTotalBought(totalBought += 1)
			setTotalAmount(totalAmount += price)		
	}

	function lessenProduct(){
		if(totalBought > 0){
			setTotalBought(totalBought -= 1)
			setTotalAmount(totalAmount -= price)			
		} 
	}

	const SuccessAlertUpdate = () => {
		Swal.fire({
			title:'FarmersPH.',
			text: 'Successfully Added to Cart',
			icon: 'success',
			showConfirmButton:false
		})	
	}

	const ErrorAlert = () => { 
		Swal.fire({
			title:'FarmersPH.',
			text: 'Failed to Add to Cart',
			icon: 'error',
			confirmButtonText: 'Try again'
		})
	}

	const addToCart = () => {
		const userId = user.id
		const token = localStorage.getItem('token')

		fetch('https://safe-bastion-71965.herokuapp.com/user/purchase', {
			method: 'POST',
			headers:{
				'Content-Type':'application/json',
				Authorization: `Bearer ${token}`
			},
			body: JSON.stringify({
				productId: _id,
				numberProductsBought: totalBought,
				price: totalAmount
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data){
				SuccessAlertUpdate()
				setTotalBought(0)
				setTotalAmount(0)
				setCounter(counter + 1)
				history('/products');
			} 
			else ErrorAlert()
		})
	}
	return <Fragment>
		<div className="product-detail-container">
			<Row>
				<Col>
					<div className="product-detail__BoxImg">
						{<Image src={imageUpload} className="product-detail__img"/>}
					</div>
				</Col>
				<Col>
					<div className="product-detail__content">
						<div className="product-detail__name-price">
							<p className="product-detail__name">{name}</p>
							<p className="product-detail__price">₱ {price}</p>
						</div>
						<p className="product-detail__description">{description}</p>
						<p className="product-detail__availableStocks">Available Stocks: <span className="product-detail__stocked">{availableStocks} pcs</span></p>
						<p className="product-detail__quantity_title">Quantity</p>
						<div className="product-detail__quantity-box">
							<div className="product-detail__quantity">
							<Button size="sm" variant="dark" className="product-detail__less" onClick={lessenProduct} disabled={totalBought > 0? false:true}>-</Button>
							<p className="product-detail__howMany">{totalBought}</p>
							<Button size="sm" variant="dark" className="product-detail__more" onClick={addProduct} disabled={totalBought < availableStocks? false: true}>+</Button>
							</div>
							<p className="product-detail__ttlAmountTitle">Total Amount:<span> ₱{totalAmount} </span></p>
						</div>
						<div className="product-detail__process">
							{user.id === null? <Button size="sm"  as={Link} to='/login' exact="true" className="product-detail__login">Login</Button>:	
								<Fragment>
									<Button size="sm" variant="light" className="product-detail__buyNowBtn" disabled={(totalBought)?false:true}>Buy Now</Button>	
									<Button size="sm" variant="light" onClick={addToCart} className="product-detail__addToCartBtn" disabled={(totalBought)?false:true}>Add to Cart</Button>	
								</Fragment>
							}
						</div>
					</div>
				</Col>
			</Row>
{/*		<Rating/>*/}
		</div>
	</Fragment>
}