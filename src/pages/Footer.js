import '../styles/Footer.css';

import { Row, Col, Nav} from 'react-bootstrap';
import { Fragment } from 'react';
import { Link } from 'react-router-dom';

// Fontawesome
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faFacebookF, faInstagram, faTwitter, faLinkedinIn } from "@fortawesome/free-brands-svg-icons"


export default function Footer(){
	return <Fragment>
		<footer className="footer">
			<Row className="footer-box">
				<Col md="5">
					<div>
						<p className="footer-title">Farmers<span className="footer-title__country">PH</span>.</p>
						<p className="footer-content">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </p>
					</div>
				</Col>
				<Col md="4">
					<div>
						<p className="footer-title">Useful Links</p>
						<Nav className="">
					      <Nav.Link as={Link} to="/" exact="true" className="footer-link" >Home</Nav.Link>
					      <Nav.Link as={Link} to="/about" exact="true" className="footer-link">About</Nav.Link>
					      <Nav.Link as={Link} to="/products" exact="true" className="footer-link">Products</Nav.Link>
						</Nav>
					</div>
				</Col>
				<Col>
					<div>
						<p className="footer-title">Let Us Connect</p>
						<p className="footer-phone-number">(+63) 912-0403-489</p>
						<p className="footer-email">farmers.ph@gmail.com</p>
					</div>
					<div className="footer-soc">
						<FontAwesomeIcon className="footer-soc-icon" icon={faFacebookF}/>
						<FontAwesomeIcon className="footer-soc-icon" icon={faInstagram}/>
						<FontAwesomeIcon className="footer-soc-icon" icon={faTwitter}/>
						<FontAwesomeIcon className="footer-soc-icon" icon={faLinkedinIn}/>
					</div>
				</Col>
			</Row>
			<div className="footer-copyright">
				<p className="footer-copyright__content">Copyright. All Rights Reserved</p>
				<p className="footer-copyright__content">FarmersPH.2021</p>
			</div>
		</footer>
	</Fragment>
}

