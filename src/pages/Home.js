import '../styles/Home.css';
import { Fragment, useState, useEffect } from 'react';
import Banner from './Banner';
import Cards from '../components/Cards';
import { Container, Row, Col, Button } from 'react-bootstrap';



export default function Home(){

/*	let [isLoaded, setIsLoaded] = useState(4);*/
	const [products, setProducts] = useState([])

	useEffect(() => {
		fetch('https://safe-bastion-71965.herokuapp.com/product',{
			method: 'GET',
			headers: { 'Content-Type':'application/json'}, 
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
			setProducts(data);
		})	
	},[])

	const mostBuyedProducts = products.map((item) => {
			// console.log(item.productImage)
			if(item.productImage !== undefined) {
			let image = `https://safe-bastion-71965.herokuapp.com/${item.productImage}`	
			console.log(image)
			
		if(item.purchaser.length > 10){
			return <Col>
				<Cards  id={item.id} title={item.name} price={item.price} image = {image} />	
			</Col>
		}
	}
	}) 


/*	function loadMore(){
		setIsLoaded(isLoaded += 4)
	}

	function loadLess(){
		setIsLoaded(isLoaded -= 4)
	}
	*/
	return <Fragment>
		<Banner/>
		<Container className="products-container">
			<p className="home-products__header">Most Selling Porducts</p>
			<Row>
				{mostBuyedProducts}
			</Row>
			{/*{isLoaded <= Card.length? <Button variant="primary" onClick={loadMore} className="button-loadMore" size="sm">Load More</Button> : <Button variant="danger" onClick={loadLess} className="button-loadless" size="sm">show less</Button>}*/}
		</Container>
	</Fragment>
}