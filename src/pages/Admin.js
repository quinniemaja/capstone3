import '../styles/Admin.css'

import {useState, useEffect, useContext, Fragment} from 'react';
import {Container, Row, Col, Card, Table, ProgressBar, Button} from 'react-bootstrap';
import {Link, Navigate} from 'react-router-dom'
import UserContext from '../userContext';


import RecentOrders from './RecentOrders';
import ProductInventory from './ProductInventory';
import TopSelling from './TopSelling';
import UserInventory from './UserInventory';
import OrderTable from './Order';





export default function Admin() {

	const {user} = useContext(UserContext);
	const today = new Date().toLocaleDateString('en-us', { weekday:"long", year:"numeric", month:"long", day:"numeric"});

	const [userCount, setUserCount] = useState(0);
	const [productCount, setProductCount] = useState(0);
	const [soldItemCount, setSoldItemCount] = useState(0);


	useEffect(() =>{
		fetch('https://safe-bastion-71965.herokuapp.com/product', {
			headers: {
				'Content-Type': 'application/json',

			}
		})
		.then(res => res.json())
		.then(prod => {
			// console.log(prod)

			setProductCount(prod.length);
		})

		fetch('https://safe-bastion-71965.herokuapp.com/user', {
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(users => {
			// console.log(users)

			setUserCount(users.length)
		})

		fetch('https://safe-bastion-71965.herokuapp.com/user/orders', {
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(orders => {

			const sold = orders.filter(product => product.status !== 'pending').sort().reverse();
			console.log(sold);
			setSoldItemCount(sold.length)
		})

	},[productCount, userCount])

	// console.log(productCount);

	

	return (
		({productCount} !== 0) ?
		<Fragment>
		<div  className = 'd-flex'>
			<Col xs ={2} className = 'adminColumn'></Col>
			<Col xs ={10}>
				<Row className = 'admin_gauge'>
					<Col sm = {4}  >
					<Card className = 'admin_gauge_box'>
						<Card.Title className = 'gauge_number'> {userCount}+ </Card.Title>
						<Card.Subtitle className ='gauge_number_sub'> Users </Card.Subtitle>	
					</Card> 
					</Col>
					<Col sm = {4}>
					<Card className = 'admin_gauge_box'>
						<Card.Title className = 'gauge_number'>{productCount}+</Card.Title>
						<Card.Subtitle className ='gauge_number_sub'> Products </Card.Subtitle>	
					</Card>
					</Col>
					<Col sm = {4} >
					<Card  className = 'admin_gauge_box'>
						<Card.Title className = 'gauge_number'> {soldItemCount}+ </Card.Title>
						<Card.Subtitle className ='gauge_number_sub'> Sold </Card.Subtitle>	
					</Card>
					</Col>
				</Row>
			
				<Row className = 'mt-4 justify-content-around'>
					<Col sm = {5} className = 'mx-3'>
					<h1 className = 'font-weight-bold'>Recent Orders</h1>
					<p>{today}</p>
					<RecentOrders/>
					</Col>
					<Col sm = {4} className = 'mx-3'>
					<h1 className = 'font-weight-bold'>Top Selling</h1>
					<p>{today}</p>
					<TopSelling/>
					</Col>
				</Row>
		

				<Row>
					<Col sm={11} className = 'mx-auto my-5 '>	
					<div>
					<h1 className = 'Prod_inventory_header'>Order List</h1>
					<div className = 'Product_year'>2021</div>
					</div >
					<Table responsive = 'xl' striped hover>
					<thead>
						<tr className ='Prod_header'>
						<th>Transaction Date</th>
						<th>Order ID</th>
						<th>Product ID</th>
						<th>Product Name</th>
						<th>Qty</th>
						<th>Price</th>
						<th>Customer</th>
						<th>Status</th>
						</tr>
						</thead>
						<tbody>
						<OrderTable/>	
						</tbody>
					</Table>
					</Col>
				</Row>
				<Row>
					<Col sm={11} className = 'mx-auto my-4 '>	
					<h1 className = 'Prod_inventory_header'>Product List</h1>
					<div>
					<div className = 'Product_year'>2021</div>
					</div>
					<Table responsive = 'lg'>
					<thead>
						<tr className ='Prod_header'>
						<th>Item</th>
						<th>Type</th>
						<th>Stock</th>
						<th>Price</th>
						<th>Edit/Archived</th>
						</tr>
						</thead>
					<ProductInventory/>
					</Table>
					
					<div>
					<Button type ='submit' variant = 'light' className = 'addProduct_btn mx-3' as = {Link} to = "/addProduct"> Add Product</Button>
					
					</div>
					</Col>
				</Row>
				<Row>
					<h1 className = 'User_list_header'>List of All Users</h1>
					<Col sm ={11} className = 'mx-auto my-4 '>	
					<Table responsive = 'sm'>
						<thead>
						<tr className ='Prod_header'>
						<th>Email Address</th>
						<th>Type</th>
						<th>Status</th>
						<th>Set Admin</th>
						</tr>
						</thead>
					<UserInventory/>
					</Table>
					</Col>
				</Row>
			</Col>
		</div>
		</Fragment>
		:
		<Fragment> 
		<Navigate to = '/addProduct'/>
		</Fragment>
		
		
		
	)
}