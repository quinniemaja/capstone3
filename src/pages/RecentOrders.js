import {Fragment, useState, useEffect} from 'react'
import Table from '../components/RecentOrdersTable'

export default function RecentOrders () {

	const [item, setItem] = useState([]);

	useEffect(() => {
		fetch('https://safe-bastion-71965.herokuapp.com/user/orders',{
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(item => {
			// console.log(item) 
			const itemFilter = item.filter(orders => orders.status === 'pending')
			const sortByDate = itemFilter.sort((a,b) => b.createdOn - a.createdOn).reverse();
			console.log(sortByDate);
			setItem(sortByDate.map(items => {
				return (
					<Table keyOrders = {items.id} orders = {items}/>
					)
			}))
		})	

	}, [])
	return (

		<Fragment>
			{item}
		</Fragment>


	)
}