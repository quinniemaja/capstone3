import '../styles/Banner.css';
import { Image, Button } from 'react-bootstrap';
import { Fragment } from 'react';

export default function Banner(){
	return <Fragment>	
		<div className="banner-section">
			<div className="banner-box__imageBox">
				<Image src="/images/banner.jpeg" className="banner-image"/>
			</div>
		</div>
{/*		<div class="banner-header">
			<p class="banner-header__title">Help our fellow farmers to sell, buy and value their products</p>
			<Button className="banner-button__register" variant="warning">Register Now</Button>
		</div>*/}
	</Fragment>
}