
import {useState, useEffect, useContext, Fragment} from 'react';
import Table from '../components/UserInventoryTable';
import UserContext from '../userContext';

export default function UserInventory() {

	const {counter, setCounter} = useContext(UserContext); 
	const [userList, setUserList] = useState([]);


	useEffect(() => {
		fetch('https://safe-bastion-71965.herokuapp.com/user', {
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			setUserList(data.map(users => {
				return (
					<Table keyUser = {users.id} userProp = {users} pageReload={setCounter}/>
					)
			}))
		})
	}, [])
	console.log(userList);

	return (
		<Fragment>
			{userList}
		</Fragment>


	)
}