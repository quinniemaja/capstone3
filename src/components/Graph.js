
import {Fragment} from 'react'
import {ProgressBar} from 'react-bootstrap'

export default function Graph ({stocks}) {

	const {stock, name} = stocks



	return (
		<Fragment>
		<div sm = {11}>
			<div className = 'inventory_barchart' >
			<span className =' px-2'>{name}</span><ProgressBar className = 'progress_bar' variant = 'success'  now ={stock} label= {`${stock}`}/>
			</div>
			
		</div>
		</Fragment>

	)
}