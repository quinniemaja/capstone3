import '../styles/TopSelling.css'

import {useState, useEffect} from 'react';
import {Table} from 'react-bootstrap';


export default function TopSelling ({topItems}) {


	const [count, setCount] = useState(0);
	const [orderCount, setOrderCount] = useState(0);

	const {name, stocked, price, purchaser, _id} = topItems

	// console.log(purchaser.length)

	useEffect(()=> {
		fetch('https://safe-bastion-71965.herokuapp.com/user/orders',{
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			// console.log(data)
			const check = data.filter(item => {
				return item.productId === _id && item.status !== 'pending'});
			
			setOrderCount(check.length);

		})
	}, [])

	return (

		<Table>
		  <tbody>
		    <tr>
		      <td>
		      	<span className = 'topSelling_item'>{name}</span>
		      	<span className = 'ml-4'>(</span>
		      	<span>No. of Checkouts: </span>
		      	<span>{orderCount}</span>
		      	<span>)</span>
		      </td>
		    </tr>
		  </tbody>
		</Table>

	)
}