import '../styles/RecentOrders.css'


import {useState, useEffect} from 'react';
import {Table} from 'react-bootstrap';


export default function RecentOrders ({orders}) {

	const {userId, productName, productId, qty, price, status} = orders

	const [buyer, setBuyer] = useState([]);
	
    useEffect(() => {
    	fetch(`https://safe-bastion-71965.herokuapp.com/user/profile/${userId}`, {
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			
			if(data){
      		setBuyer(() => {
      			return {
      				firstname: data.fullName[0].givenName,
      				lastname: data.fullName[0].familyName
      			}
      		})
      }})
    }, [])
	// console.log(buyer)

	

	return (

		<Table>
		  <tbody>
		    <tr>
		      <td>
		      	<h2 className = 'Latest_item_name'>{productName}</h2>
		      	<span>Buyer: </span>
		      	<span>{buyer.firstname + ' ' +buyer.lastname}</span>
		      	<span className = 'ml-2'>(</span>
		      	<span>{ qty} pcs</span>
		      	<span>/</span>
		      	<span>&#8369; {price}</span>
		      	<span>)</span>
		      </td>
		      <td className = 'pt-5'>{status}</td>
		    </tr>
		  </tbody>
		</Table>

	)
}