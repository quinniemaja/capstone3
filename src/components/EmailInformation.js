import '../styles/GeneralInformation.css';
import { Fragment, useState } from 'react';
import { Form, Button } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'; 
import { faEye  } from '@fortawesome/free-solid-svg-icons';
import { useNavigate } from 'react-router-dom'; 
import Swal from 'sweetalert2';
export default function EmailInformation({nextStep, prevStep, handleChange, values}){
	let disableButton = true;
	let [passwordVisible, setPasswordVisible] = useState("password")
	let [passwordVisible2, setPasswordVisible2] = useState("password")
	let {firstname, lastname, gender, birthday, email, password, password2, lotNumber, barangay, city, province, country} = values
	
	const history = useNavigate()

	if((email && password && password2) && (password === password2)) {
		disableButton = false
	}else disableButton = true
	
	const Previous = (e) => {
		e.preventDefault()
		prevStep()
	}
	const seePassword = () => {
		if(passwordVisible === "password") setPasswordVisible("text")
		else setPasswordVisible("password")
	}
	
	const seePassword2 = () => {
		if(passwordVisible2 === "password") setPasswordVisible2("text")
		else setPasswordVisible2("password")
	}

	const SuccessAlert = () => {
		Swal.fire({
			title:'FarmersPH.',
			text: 'Successfully created the Account',
			icon: 'success',
			confirmButtonText: 'Proceed'
		})	
	}

	const ErrorAlert = () => { 
		Swal.fire({
			title:'FarmersPH.',
			text: 'Failed to create the account',
			icon: 'error',
			confirmButtonText: 'Try again'
		})
	}

	const EmailExistAlert = () => {
		Swal.fire({
			title:'FarmersPH.',
			text: 'Failed to create the account! Email exist!',
			icon: 'error',
			confirmButtonText: 'Try again'
		})		
	}

	const SignUp = (e) => {
		e.preventDefault();
		fetch('https://safe-bastion-71965.herokuapp.com/user/check-email',{
			method:'POST',
			headers:{'Content-Type':'application/json'},
			body: JSON.stringify({ email: email })
		})
		.then(res => res.json())
		.then(data => {
			if(data){
				fetch('https://safe-bastion-71965.herokuapp.com/user/register',{
					method:'POST',
					headers:{'Content-Type':'application/json'},
					body: JSON.stringify({
						firstname: firstname,
						lastname: lastname,
						lotNumber: lotNumber,
						barangay: barangay, 
						city: city, 
						province: province, 
						country: country,
						gender: gender,
						birthday: birthday,
						email: email,
						password: password
					}),
				})
				.then(res => res.json())
				.then(data => {
					console.log(data)
					if(data){
						SuccessAlert()
						history("/login");
					} else{
						ErrorAlert()
						history("/signup");
					} 
				})
			}else{
				EmailExistAlert()
				history("/signup");				
			}
		})
	}


	return <Fragment>
		<Form>
		<div className="signup-general-information">
			<p className="sign-up__header">Email Information</p>
			<Form.Group className="mb-3">
			    <Form.Label className="form-label-header">Email</Form.Label>
			    <Form.Control 
			    	onChange={handleChange} 
			    	size="sm" 
			    	type="email" 
			    	name="email"
			    	value={email}
			    	placeholder="Please enter your email" />
			  </Form.Group>
			<Form.Group className="mb-3" >
			    <Form.Label className="form-label-header">Password</Form.Label>
			    <div className="password-container">
			    <Form.Control 
			    	onChange={handleChange} 
			    	size="sm" 
			    	type={passwordVisible} 
			    	name="password"
			    	value={password}
			    	placeholder="Please enter password" />
				  <Button onClick={seePassword} value="passwordBtn" size="sm" className="form-button__seePassword" ><FontAwesomeIcon icon={faEye}/></Button>
				</div>
			  </Form.Group>
			<Form.Group className="mb-3">
			    <Form.Label className="form-label-header">Re-enter Password</Form.Label>
			    <div className="password-container">
				    <Form.Control
				    	onChange={handleChange} 
				    	size="sm" 
				    	type={passwordVisible2} 
				    	name="password2"
				    	value={password2}
				    	placeholder="Please re-enter password" />
				  <Button onClick={seePassword2} name="" size="sm" className="form-button__seePassword" ><FontAwesomeIcon icon={faEye}/></Button>			    	
			    </div>
			  </Form.Group>
		</div>
		<div className="process-btn-container">
			<Button className="prev-btn" onClick={ Previous }>Go Back</Button>
			<Button className="signup-btn" onClick={ SignUp } disabled={disableButton? true: false}>Sign up</Button>
		</div>
		</Form>
	</Fragment>	
}