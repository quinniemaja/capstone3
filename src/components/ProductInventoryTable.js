import {Fragment, useEffect, useState, useContext} from 'react';
import {Table, Button, Modal, Form, Row, Col} from 'react-bootstrap';
import {Link, useNavigate} from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../userContext';

import '../styles/ProductInventory.css'


// Fontawesome
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'; 
import { faEdit } from '@fortawesome/free-solid-svg-icons'; 
import { faToggleOn } from '@fortawesome/free-solid-svg-icons'; 
import { faToggleOff } from '@fortawesome/free-solid-svg-icons'; 


export default function ProductInventory ({product, reload}) {
	
	const {counter, setCounter} = useContext(UserContext); 
	const history = useNavigate();
	const {name, type, price, createdOn, stocked, _id, isActive} = product
	const [buttonStatus, setButtonStatus] = useState('Active');
	const [editName, setEditName] = useState('')
	const [editType, setEditType] = useState('')
	const [editDescription, setEditDescription] = useState('')
	const [editNumberOfStocks, setEditNumberOfStocks] = useState('')
	const [editPrice, setEditPrice] = useState(0)
	// const [editAvailable, setEditAvailable] = useState('')

	const dateAdded = new Date(createdOn).toLocaleDateString('en-US');
	// States to open/close modals
	const [showEdit, setShowEdit] = useState(false);
	const [showAdd, setShowAdd] = useState(false);


	//edit product

	function openEdit(id) {
		fetch(`https://safe-bastion-71965.herokuapp.com/product/${_id}`, {
			headers: {
				'Content-Type': 'application/json'	
			}
		})
		.then(res => res.json())
		.then(prod => {
			// console.log(prod)
			setEditName(prod.name);
			setEditDescription(prod.description);
			setEditType(prod.type);
			setEditNumberOfStocks(prod.stocked);
			setEditPrice(prod.price)
		
		})
		setShowEdit(true);
		
	};
// close modal
	function closeEdit() { 
		setShowEdit(false);
			setEditName('');
			setEditDescription('');
			setEditType('');
			setEditNumberOfStocks('');
			setEditPrice(0)
	}

	function editProduct (e, id) {
		e.preventDefault();
		fetch(`https://safe-bastion-71965.herokuapp.com/product/update/${_id}`, {
			method: 'PATCH',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
				
			},
			body: JSON.stringify({
				name: editName, 
				description: editDescription,
				type: editType,
				price: editPrice,
				numberOfStocks: editNumberOfStocks
			})

		})
		.then(res => res.json())
		.then(prod => {
			// console.log(prod)
			if(prod !== 'undefined') {

				Swal.fire({
					title: 'Success!',
					icon: 'success',
					text: 'Product is Successfully Updated'
				});
				reload(counter + 1)
			history('/admin');

			} else {
				Swal.fire({
				title: 'Something went wrong',
				icon: 'error',
				text: 'Please try again.'
			})
			}
		})

	}
	const active = true
	const inActive = false

	function archiveProduct (id) {
		return fetch(`https://safe-bastion-71965.herokuapp.com/product/archive/${_id}`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json', 
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				isActive: inActive
			})
		})
		.then(res => res.json())
		.then(product => {
			// console.log(product)
			if(product.isActive == false) {
				Swal.fire({
					title:'FarmersPH.',
					text: 'Product has been archived!',
					icon: 'success',
					confirmButtonText: 'Proceed'
				});
				setCounter(counter + 1);
				return true


			} else {
				Swal.fire({
					title:'FarmersPH.',
					text: 'Something is wrong!',
					icon: 'error',
					confirmButtonText: 'Back'	
					})
				}	

		})

	}

	function activateProduct (id) {
		return fetch(`https://safe-bastion-71965.herokuapp.com/product/archive/${_id}`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json', 
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				isActive: active
			})
		})
		.then(res => res.json())
		.then(product => {
			// console.log(product)
			if(product.isActive = active) {
				Swal.fire({
					title:'FarmersPH.',
					text: 'Product has been activated!',
					icon: 'success',
					confirmButtonText: 'Proceed'
				})
				setCounter(counter + 1);
				return true
			} else {
				Swal.fire({
					title:'FarmersPH.',
					text: 'Something is wrong!',
					icon: 'error',
					confirmButtonText: 'Back'	
					})
				}
		})
	}
 	// console.log(isActive)
 	// useEffect(() => {
 	// 	if(isActive) {
 	// 		setButtonStatus('Active')
 	// 	} else {
 	// 		setButtonStatus('Inactive')
 			
 	// 	}



 	// }, [])
 
	

	return (
	
	<Fragment>
		
		
		    <tr >
		      <td >
		      <h2 className = 'Prod_item_name'>{name}</h2>
		      <span className = 'Prod_item_sub'>Added on {dateAdded}</span>
		      </td >
		      <td className = 'Prod_item_name'>{type}</td>
		      <td className = 'Prod_item_name'>{stocked}</td>
		      <td className = 'Prod_item_name' >&#8369; {price}</td>
		      <td className = 'd-flex'>
				<FontAwesomeIcon variant = 'light' className="edit_icon fa-2x mx-3" type ='submit' onClick={(()=> openEdit(_id))} icon={faEdit}/>

		      	{/*<Button type ='submit' variant = 'light' className = 'editProduct_btn mx-3' onClick={(() => openEdit(_id))}> Edit Product</Button>*/}
		      		{(isActive) ?
		      		<Fragment>
		      		<FontAwesomeIcon className="active_icon fa-2x mx-3" type ='submit' onClick={archiveProduct} icon={faToggleOn}/>
		      		{/*<Button type ='submit' variant = 'light' className = 'activeProduct_btn mx-3' onClick= {archiveProduct}>{buttonStatus}</Button>	*/}
		      		</Fragment>
		      		:
		      		<Fragment>
		      		<FontAwesomeIcon className="inactive_icon fa-2x mx-3" type ='submit' onClick={activateProduct} icon={faToggleOff}/>
{/*
					<Button type ='submit' variant = 'light' className = 'inActiveProduct_btn mx-3' onClick= {activateProduct}>{buttonStatus}</Button>*/}

					</Fragment>}	
		      </td>
		    </tr>
		  
		

		<Modal show={showEdit} onHide={closeEdit}>
				<Form onSubmit={e => editProduct(e, _id)}>
					<Modal.Header closeButton>
						<Modal.Title>Edit Product Information</Modal.Title>
					</Modal.Header>
					<Modal.Body>	
					<div className="edit-prod-information">
					<Row>
					<Col>
					<Form.Group className="mb-3">
					<Form.Label className="form-label-header">Product Name</Form.Label>
					<Form.Control 
					onChange={e => setEditName(e.target.value)} 
					size="sm" 
					type="text" 
					value = {editName}
					required/>
					</Form.Group>
					</Col>
					<Col>
					<Form.Group className="mb-3" controlId="formBasicEmail">
					<Form.Label className="form-label-header">Type</Form.Label>
					<Form.Control 
					onChange={e => setEditType(e.target.value)} 
					size="sm" 
					type="text" 
					value = {editType}
					required />
					</Form.Group>
					</Col>
					</Row>
					<Form.Group className="mb-3" controlId="formBasicEmail">
					<Form.Label className="form-label-header">Description</Form.Label>
					<div className="signup-address-input">
					<Form.Control  
					onChange={e => setEditDescription(e.target.value)}  
					size="sm" as="textarea" 
					value = {editDescription}
					required />
					</div>
					</Form.Group>
					<Row>
					<Col>
					<Form.Group className="mb-3" controlId="formBasicEmail">
					<Form.Label className="form-label-header">Quantity</Form.Label>
					<Form.Control 
					onChange={e => setEditNumberOfStocks(e.target.value)} 
					size="sm" 
					type="text" 
					value= {editNumberOfStocks}
					required/>
					</Form.Group>
					</Col>
					<Col>
					<Form.Group className="mb-3" controlId="formBasicEmail">
					<Form.Label className="form-label-header">Price</Form.Label>
					<Form.Control 
					onChange={e => setEditPrice(e.target.value)} 
					size="sm" 
					type="text" 
					value = {editPrice}
					required />
					</Form.Group>
					</Col>
					</Row> 
					<Form.Group controlId="formFileSm" className="mb-3">
					<Form.Label>Upload file for Image</Form.Label>
					<Form.Control type="file" size="sm" />
					</Form.Group>
					</div>
					</Modal.Body>
					<Modal.Footer>
						<Button variant="secondary" onClick={closeEdit}>Close</Button>
						<Button variant="success" type="submit">Submit</Button>
					</Modal.Footer>
				</Form>
			</Modal>

	
		
	</Fragment>
	

	)
}