import {useState, useEffect, useContext, Fragment} from 'react'
import {useNavigate} from 'react-router-dom'
import {Table, Button} from 'react-bootstrap';
import Swal from 'sweetalert2';
import UserContext from '../userContext';

export default function OrderTable ({ordered}) {

	const {_id, productId, productName, userId, qty, price, status, createdOn} = ordered
	const [buyer, setBuyer] =useState('');
	const orderDate = new Date(createdOn).toLocaleDateString('en-US');

	useEffect(() => {

    	fetch(`https://safe-bastion-71965.herokuapp.com/user/profile/${userId}`, {
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			
			if(data){
      		setBuyer(() => {
      			return {
      				firstname: data.fullName[0].givenName,
      				lastname: data.fullName[0].familyName
      			}
      		})
      }})

    }, [])

	return (

		  <tr className = 'User_item_row'>
		      <td className = 'user_item_name'>{orderDate}</td>
		      <td className = 'user_item_name'>{_id}</td>
		      <td className = 'user_item_name'>{productId}</td>
		      <td className = 'user_item_name'>{productName}</td>
		      <td className = 'user_item_name'>{qty}</td>
		      <td className = 'user_item_name'>{price}</td>
		      <td className = 'user_item_name'>{buyer.firstname + ' ' +buyer.lastname}</td>
		      <td className = 'user_item_name'>{status}</td>
		    </tr>
	)
}